#!/usr/bin/python3
import statistics
from sys import exit, argv, version_info

SAMPLE_COUNT = 500
CONV = 1000
NUMVALS = 8

NAMES = [
    "Start -> Get",
    "Get",
    "Get -> PID",
    "PID",
    "PID -> Set",
    "Set",
    "SET -> TX",
    "Total"
    ]

def main():
    if (len(argv) < 2):
        print("Usage: %s file" % (argv[0],))
        return 1
    try:
        f = open(argv[1], 'r')
    except:
        print("Unable to open file %s" % (argv[1],))
        return 2

    f.readline()
    vals = []
    for i in range(SAMPLE_COUNT):
        vals.append({'total':0, 'done':0, 'valid':False})
    i = 0
    while (i < SAMPLE_COUNT):
        try:
            temp = f.readline().strip('\n')
            if not temp:
                print("File ended before %d samples found! (%d)" % 
                    (SAMPLE_COUNT,i))
                return
            temp = temp.split(',')
            temp[0] = int(temp[0])
            if (temp[1] == '01' and temp[2] == '1'):
                vals[i]['total'] = 0
                vals[i]['done'] = 0
                vals[i]['tx-started'] = False
                vals[i]['valid'] = True
                continue
            else:
                vals[i]['total'] = vals[i]['total'] + temp[0]

            if (temp[2] == '0' and vals[i]['valid'] == True \
                    and vals[i]['tx-started'] == False):
                vals[i]['tx-start'] = vals[i]['total']
                vals[i]['tx-diff'] = vals[i]['total'] - \
                        vals[i]['send-start']
                vals[i]['done'] += 1
                vals[i]['tx-started'] = True
            elif (temp[1] == '03'): # GET is starting
                vals[i]['recv-start'] = vals[i]['total']
            elif (temp[1] == '07'): # GET is finished
                vals[i]['recv-end'] = vals[i]['total'] -\
                        vals[i]['recv-start']
            elif (temp[1] == '0F'): # PID is starting
                vals[i]['pid-start'] = vals[i]['total']
            elif (temp[1] == '1F'): # PID is finished
                vals[i]['pid-end'] = vals[i]['total'] -\
                        vals[i]['pid-start']
            elif (temp[1] == '3F'): # SET is starting
                vals[i]['send-start'] = vals[i]['total']
            elif (temp[1] == '7F'): # SET is finished
                vals[i]['send-end'] = vals[i]['total'] -\
                        vals[i]['send-start']
                vals[i]['done'] += 1

            if (vals[i]['done'] == 2):
                i += 1

        except ValueError:
            continue
    f.close()
    prev = 0
    keys = ['recv-start', 'recv-end', 'pid-start', 'pid-end', \
            'send-start', 'send-end', 'tx-start', 'tx-diff', \
            'total']
    for i in keys:
        temp = []
        for j in range(SAMPLE_COUNT):
            temp.append(vals[j][i])
        print(i)
        mean = statistics.mean(temp)
        sd = statistics.stdev(temp)
        if (len(argv) > 2 and argv[2] == "-t"):
            if (i == 'tx-diff'):
                continue
            else:
                print("Mean: %fus" % ((mean / CONV),))
        else:
            if 'start' in i:
                continue
            print("Min: %fus\nMax: %fus" % (min(temp) / CONV, 
                    max(temp) / CONV))
            print("Mean: %fus\nstdev: %fus" % (mean / CONV, sd / CONV))

        if (version_info[1] >= 6):
            print("Harmonic Mean: %fus" % 
                    (statistics.harmonic_mean(vals[i]) / CONV)) 
        if (len(argv) > 2 and argv[2] == "-y"):
            final_vals = [x for x in temp if (x > mean - 2 * sd)]
            final_vals = [x for x in final_vals if (x < mean + 2 * sd)]
            print("Outliers removed:") 
            print("Mean: %fus\nstdev: %fus" % 
                    (statistics.mean(final_vals) / CONV,
                    statistics.stdev(final_vals) / CONV))
        elif (len(argv) > 2 and argv[2] == "-m"):
            print("Max: %fus\nIndex: %d" % (max(temp) / CONV, 
                    temp.index(max(temp))))
            print("Min: %fus\nIndex: %d" % (min(temp) / CONV, 
                    temp.index(min(temp))))
        print()
    return 0

if __name__ == '__main__':
    exit(main())
