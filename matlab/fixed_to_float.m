function [float_val] = fixed_to_float(fixed_val, fixed_bits)
%fixed_to_float Converts fixed point to floating point
%   16 bit fixed point value with fixed_bits points of precision
float_val = double(double(fixed_val) / double(bitsll(1, fixed_bits)));
end
