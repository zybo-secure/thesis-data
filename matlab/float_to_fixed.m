function [fixed_val] = float_to_fixed(float_val, fixed_bits)
%float_to_fixed Converts floating point to fixed point
%   16 bit fixed point value with fixed_bits points of precision
fixed_val = int16(round(float_val * (bitsll(1,fixed_bits))));
end

