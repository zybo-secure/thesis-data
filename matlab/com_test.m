FIXED_POINT_BITS = 5;

instrreset
miso = serial("COM4", 'BaudRate', 57600, 'Terminator', 0);
mosi = serial("COM5", 'BaudRate', 57600, 'Terminator', 0);

a = float_to_fixed(-25, FIXED_POINT_BITS);
sprintf("0x%04x", typecast(a, 'uint16'))
b = float_to_fixed(22, FIXED_POINT_BITS);
sprintf("0x%04x", typecast(b, 'uint16'))

data = [int16(2),a,b];

fopen(miso);
fopen(mosi);
fwrite(mosi, data, 'int16');
while (miso.BytesAvailable == 0);
end
numbytes = fread(miso, 1, 'int16')
new_data = fread(miso, numbytes, 'int16')

a_new = fixed_to_float(new_data(1), FIXED_POINT_BITS)
b_new = fixed_to_float(new_data(2), FIXED_POINT_BITS)

fclose(miso);
fclose(mosi);