# thesis-data

Data, scripts and graphics gathered for thesis and paper with patterson

overall-data - Timing data of the overall control loops without internal timing. Note that the first line is erroreous and should be ignored. 
scripts/overall.py - Script to evaluate the mean and std devation of data from the overall-data directory. Use -y to evaluate the mean of the dataset after remove values outside the standard deviation.

For collecting timing data:
start controller program on zybo (taskset -c 1 controller)
start logic analyzer collection
start matlab script

export after long periods of 03 disappear (wait for init timings to be gone)
to export - click GPIO, then UART, then disable line numbers and set time stamp to delta

timing.py:
-t - use for timing diagrams (shift decimal 1 place left for latex)
(no arg) - use for tables

